USE [master]
GO
/****** Object:  Database [Parcial]    Script Date: 30/9/2020 11:16:44 ******/
CREATE DATABASE [Parcial]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Parcial', FILENAME = N'D:\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Parcial.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Parcial_log', FILENAME = N'D:\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Parcial_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Parcial] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Parcial].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Parcial] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Parcial] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Parcial] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Parcial] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Parcial] SET ARITHABORT OFF 
GO
ALTER DATABASE [Parcial] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Parcial] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Parcial] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Parcial] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Parcial] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Parcial] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Parcial] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Parcial] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Parcial] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Parcial] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Parcial] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Parcial] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Parcial] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Parcial] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Parcial] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Parcial] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Parcial] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Parcial] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Parcial] SET  MULTI_USER 
GO
ALTER DATABASE [Parcial] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Parcial] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Parcial] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Parcial] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Parcial] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Parcial] SET QUERY_STORE = OFF
GO
USE [Parcial]
GO
/****** Object:  Table [dbo].[Conceptos]    Script Date: 30/9/2020 11:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Conceptos](
	[Id] [int] NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
	[Porcentaje] [nvarchar](50) NOT NULL,
	[Signo] [int] NOT NULL,
 CONSTRAINT [PK_Conceptos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empleados]    Script Date: 30/9/2020 11:16:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empleados](
	[Id] [int] NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Apellido] [nvarchar](50) NOT NULL,
	[CUIL] [bigint] NOT NULL,
	[Legajo] [bigint] NOT NULL,
	[FechaAlta] [date] NOT NULL,
	[NombreApellido] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Empleados] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recibos]    Script Date: 30/9/2020 11:16:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recibos](
	[Id] [int] NOT NULL,
	[Id_Empleado] [int] NOT NULL,
	[SueldoBruto] [decimal](18, 0) NOT NULL,
	[SueldoNeto] [decimal](18, 0) NOT NULL,
	[Fecha] [date] NOT NULL,
	[Mes] [int] NOT NULL,
	[Año] [int] NOT NULL,
	[TotalDescuentos] [int] NOT NULL,
 CONSTRAINT [PK_Recibos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recibos_Detalle]    Script Date: 30/9/2020 11:16:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recibos_Detalle](
	[Id] [int] NOT NULL,
	[Id_Empleado] [int] NOT NULL,
	[Id_Recibo] [int] NOT NULL,
	[Id_Concepto] [int] NOT NULL,
	[Fecha] [date] NOT NULL,
 CONSTRAINT [PK_Recibos_Detalle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Conceptos] ([Id], [Descripcion], [Porcentaje], [Signo]) VALUES (1, N'Obra Social', N'2', 1)
GO
INSERT [dbo].[Conceptos] ([Id], [Descripcion], [Porcentaje], [Signo]) VALUES (2, N'Resolución 2561', N'1', 1)
GO
INSERT [dbo].[Conceptos] ([Id], [Descripcion], [Porcentaje], [Signo]) VALUES (3, N'Bonificación Gremio', N'3', 0)
GO
INSERT [dbo].[Conceptos] ([Id], [Descripcion], [Porcentaje], [Signo]) VALUES (4, N'Ley 123456', N'5', 0)
GO
INSERT [dbo].[Empleados] ([Id], [Nombre], [Apellido], [CUIL], [Legajo], [FechaAlta], [NombreApellido]) VALUES (1, N'Juan Manuel', N'Bustamante', 26445874416, 44587441, CAST(N'2020-09-30' AS Date), N'Juan Manuel Bustamante')
GO
INSERT [dbo].[Empleados] ([Id], [Nombre], [Apellido], [CUIL], [Legajo], [FechaAlta], [NombreApellido]) VALUES (2, N'Marcela', N'Guiterrez', 24548441444, 54844144, CAST(N'2019-09-30' AS Date), N'Marcela Guiterrez')
GO
INSERT [dbo].[Empleados] ([Id], [Nombre], [Apellido], [CUIL], [Legajo], [FechaAlta], [NombreApellido]) VALUES (3, N'Sabino', N'Escalante', 26574147894, 57414789, CAST(N'2020-09-01' AS Date), N'Sabino Escalante')
GO
INSERT [dbo].[Recibos] ([Id], [Id_Empleado], [SueldoBruto], [SueldoNeto], [Fecha], [Mes], [Año], [TotalDescuentos]) VALUES (1, 1, CAST(40000 AS Decimal(18, 0)), CAST(42000 AS Decimal(18, 0)), CAST(N'2020-09-30' AS Date), 9, 2020, 3)
GO
INSERT [dbo].[Recibos] ([Id], [Id_Empleado], [SueldoBruto], [SueldoNeto], [Fecha], [Mes], [Año], [TotalDescuentos]) VALUES (2, 2, CAST(50000 AS Decimal(18, 0)), CAST(48500 AS Decimal(18, 0)), CAST(N'2020-09-30' AS Date), 9, 2020, 3)
GO
INSERT [dbo].[Recibos] ([Id], [Id_Empleado], [SueldoBruto], [SueldoNeto], [Fecha], [Mes], [Año], [TotalDescuentos]) VALUES (3, 3, CAST(60000 AS Decimal(18, 0)), CAST(62400 AS Decimal(18, 0)), CAST(N'2020-09-30' AS Date), 9, 2020, 1)
GO
INSERT [dbo].[Recibos_Detalle] ([Id], [Id_Empleado], [Id_Recibo], [Id_Concepto], [Fecha]) VALUES (1, 1, 1, 3, CAST(N'2020-09-30' AS Date))
GO
INSERT [dbo].[Recibos_Detalle] ([Id], [Id_Empleado], [Id_Recibo], [Id_Concepto], [Fecha]) VALUES (2, 1, 1, 2, CAST(N'2020-09-30' AS Date))
GO
INSERT [dbo].[Recibos_Detalle] ([Id], [Id_Empleado], [Id_Recibo], [Id_Concepto], [Fecha]) VALUES (3, 1, 1, 1, CAST(N'2020-09-30' AS Date))
GO
INSERT [dbo].[Recibos_Detalle] ([Id], [Id_Empleado], [Id_Recibo], [Id_Concepto], [Fecha]) VALUES (4, 1, 1, 4, CAST(N'2020-09-30' AS Date))
GO
INSERT [dbo].[Recibos_Detalle] ([Id], [Id_Empleado], [Id_Recibo], [Id_Concepto], [Fecha]) VALUES (5, 2, 2, 1, CAST(N'2020-09-30' AS Date))
GO
INSERT [dbo].[Recibos_Detalle] ([Id], [Id_Empleado], [Id_Recibo], [Id_Concepto], [Fecha]) VALUES (6, 2, 2, 2, CAST(N'2020-09-30' AS Date))
GO
INSERT [dbo].[Recibos_Detalle] ([Id], [Id_Empleado], [Id_Recibo], [Id_Concepto], [Fecha]) VALUES (7, 3, 3, 4, CAST(N'2020-09-30' AS Date))
GO
INSERT [dbo].[Recibos_Detalle] ([Id], [Id_Empleado], [Id_Recibo], [Id_Concepto], [Fecha]) VALUES (8, 3, 3, 2, CAST(N'2020-09-30' AS Date))
GO
ALTER TABLE [dbo].[Recibos]  WITH CHECK ADD  CONSTRAINT [FK_Recibos_Empleados] FOREIGN KEY([Id_Empleado])
REFERENCES [dbo].[Empleados] ([Id])
GO
ALTER TABLE [dbo].[Recibos] CHECK CONSTRAINT [FK_Recibos_Empleados]
GO
ALTER TABLE [dbo].[Recibos_Detalle]  WITH CHECK ADD  CONSTRAINT [FK_Recibos_Detalle_Conceptos] FOREIGN KEY([Id_Concepto])
REFERENCES [dbo].[Conceptos] ([Id])
GO
ALTER TABLE [dbo].[Recibos_Detalle] CHECK CONSTRAINT [FK_Recibos_Detalle_Conceptos]
GO
ALTER TABLE [dbo].[Recibos_Detalle]  WITH CHECK ADD  CONSTRAINT [FK_Recibos_Detalle_Empleados] FOREIGN KEY([Id_Empleado])
REFERENCES [dbo].[Empleados] ([Id])
GO
ALTER TABLE [dbo].[Recibos_Detalle] CHECK CONSTRAINT [FK_Recibos_Detalle_Empleados]
GO
ALTER TABLE [dbo].[Recibos_Detalle]  WITH CHECK ADD  CONSTRAINT [FK_Recibos_Detalle_Recibos] FOREIGN KEY([Id_Recibo])
REFERENCES [dbo].[Recibos] ([Id])
GO
ALTER TABLE [dbo].[Recibos_Detalle] CHECK CONSTRAINT [FK_Recibos_Detalle_Recibos]
GO
USE [master]
GO
ALTER DATABASE [Parcial] SET  READ_WRITE 
GO
