﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Negocio;

namespace ParcialPractico
{
    public partial class frmEmpleados : Form
    {
        private int rowSeleccionado = 0;
        public frmEmpleados()
        {
            InitializeComponent();
        }

        private void Empleados_Load(object sender, EventArgs e)
        {
            Enlazar();
            this.WindowState = FormWindowState.Maximized;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            frmNuevoEmpleado ne = new frmNuevoEmpleado();
            ne.ShowDialog();
            Enlazar();
        }

        private void Enlazar()
        {
            grdListaEmpleados.DataSource = null;
            List<Empleado> empleados = new List<Empleado>();
            empleados = Empleado.Listar();
            grdListaEmpleados.DataSource = empleados;
            cboxEmpleados.DataSource = empleados;
            cboxEmpleados.DisplayMember = "NombreApellido";
            cboxEmpleados.ValueMember = "Id";
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            grdListaEmpleados.DataSource = null;
            List<Empleado> empleados = new List<Empleado>();
            if(chkTodos.Checked)
            {
                empleados = Empleado.Listar();
            }
            else
            {
                empleados = Empleado.Listar(int.Parse(cboxEmpleados.SelectedValue.ToString()));
            }
            grdListaEmpleados.DataSource = empleados;
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            cboxEmpleados.SelectedIndex = 0;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Empleado empleado = new Empleado();
            empleado.Id = rowSeleccionado;
            if (empleado.Eliminar() >= 1)
            {
                MessageBox.Show("Se ha eliminado el empleado con éxito!", "Empleado", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("Hubo un error eliminando el empleado", "Atención!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            Enlazar();
        }

        private void grdListaEmpleados_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            rowSeleccionado = int.Parse(grdListaEmpleados.Rows[e.RowIndex].Cells[0].Value.ToString());
        }

        private void cboxEmpleados_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            List<Empleado> empleados = new List<Empleado>();
            empleados = Empleado.Listar(int.Parse(cboxEmpleados.SelectedValue.ToString()));
            frmModificarEmpleado frmModificarEmpleado = new frmModificarEmpleado(empleados[0]);            
            frmModificarEmpleado.ShowDialog();
            Enlazar();
        }
    }    
}
