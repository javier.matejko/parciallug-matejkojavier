﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Negocio;

namespace ParcialPractico
{
    public partial class frmModificarEmpleado : Form
    {
        private Empleado empleado;
        public frmModificarEmpleado(Empleado e)
        {
            InitializeComponent();
            empleado = e;
        }

        private void frmModificarEmpleado_Load(object sender, EventArgs e)
        {
            this.txtNombre.Text = empleado.Nombre;
            this.txtApellido.Text = empleado.Apellido;
            this.txtCUIL.Text = empleado.Cuil.ToString();
            this.dtpFechaAlta.Value = empleado.FechaAlta;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtApellido.Text = txtCUIL.Text = txtLegajo.Text = txtNombre.Text = "";
            dtpFechaAlta.Value = DateTime.Today;
        }
        

        private void txtCUIL_TextChanged(object sender, EventArgs e)
        {
            txtLegajo.Text = txtCUIL.Text.Substring(2, 9);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            #region Condiciones
            if (this.txtNombre.Text == "") { MessageBox.Show("Ingrese un nombre válido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            else if (this.txtApellido.Text == "") { MessageBox.Show("Ingrese un apellido válido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            else if (this.txtCUIL.Text.Length < 13) { MessageBox.Show("Ingrese un CUIL válido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            else if (this.dtpFechaAlta.Value == DateTime.MinValue) { MessageBox.Show("Ingrese una fecha válida", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            #endregion
            else
            {
                empleado.Nombre = this.txtNombre.Text;
                empleado.Apellido = this.txtApellido.Text;
                empleado.Cuil = Int64.Parse(this.txtCUIL.Text.Replace("-", ""));
                empleado.Legajo = Int64.Parse(this.txtLegajo.Text.Replace("-", ""));
                empleado.FechaAlta = dtpFechaAlta.Value;
                empleado.NombreApellido = this.txtNombre.Text + " " + this.txtApellido.Text;
                if (empleado.Editar() == 1)
                {
                    MessageBox.Show("Empleado modificado con éxito!", "Empleado", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("Hubo un error modificando el empleado", "Atención!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                this.Close();
            }
        }
    }
}
