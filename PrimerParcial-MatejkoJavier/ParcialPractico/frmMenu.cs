﻿using ParcialPractico;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void Menu_Load(object sender, EventArgs e)
        {
        }

        private void empleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void recibosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void verEmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEmpleados frmEmpleados = new frmEmpleados();
            frmEmpleados.MdiParent = this;
            frmEmpleados.Show();
        }
        private void nuevoEmpleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNuevoEmpleado frmNuevoEmpleado = new frmNuevoEmpleado();
            frmNuevoEmpleado.ShowDialog();
        }


        private void verRecibosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRecibos frmRecibos = new frmRecibos();
            frmRecibos.MdiParent = this;
            frmRecibos.Show();
        }
        private void nuevoReciboToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNuevoRecibo frmNuevoRecibo = new frmNuevoRecibo();
            frmNuevoRecibo.ShowDialog();
        }


        private void verConceptosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConceptos frmConceptos = new frmConceptos();
            frmConceptos.MdiParent = this;
            frmConceptos.Show();
        }
        private void nuevoConceptoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNuevoConcepto frmNuevoConcepto = new frmNuevoConcepto();
            frmNuevoConcepto.ShowDialog();
        }


        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
