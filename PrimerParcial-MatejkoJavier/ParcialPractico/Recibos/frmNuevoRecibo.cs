﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Negocio;

namespace ParcialPractico
{
    public partial class frmNuevoRecibo : Form
    {
        private int descuentos = 0;
        private int bonificacion = 0;
        private List<Concepto> conceptos = new List<Concepto>();
        private List<Concepto> conceptosSeleccionados = new List<Concepto>();
        public frmNuevoRecibo()
        {
            InitializeComponent();
        }
        private void frmNuevoRecibo_Load(object sender, EventArgs e)
        {
            Enlazar();
        }
        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            cboxEmpleados.SelectedIndex = 0;
            txtSueldoBruto.Text = txtSueldoNeto.Text = "";
            dtpFecha.Value = DateTime.Today;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            #region Condiciones
            if (txtSueldoBruto.Text == "") { MessageBox.Show("Ingrese un importe válido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            else if (txtDescuentos.Text == "") { MessageBox.Show("Ingrese un concepto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            else
            {
                #endregion
                Recibo recibo = new Recibo();
                recibo.Id_Empleado = int.Parse(cboxEmpleados.SelectedValue.ToString());
                recibo.SueldoBruto = int.Parse(txtSueldoBruto.Text);
                recibo.SueldoNeto = int.Parse(txtSueldoNeto.Text);
                recibo.Fecha = dtpFecha.Value;
                recibo.Descuentos = int.Parse(txtDescuentos.Text);
                recibo.Conceptos = conceptosSeleccionados;
                if (recibo.Insertar() == 1)
                {
                    MessageBox.Show("Recibo cargado con éxito!", "Recibo", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("Hubo un error cargando el recibo", "Atención!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                this.Close();
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            frmNuevoConcepto frmNuevoConcepto = new frmNuevoConcepto();
            frmNuevoConcepto.ShowDialog();
            Enlazar();
        }

        private void btnLimpiarItems_Click(object sender, EventArgs e)
        {
        }

        private void txtSueldoBruto_TextChanged(object sender, EventArgs e)
        {
            txtSueldoNeto.Text = txtSueldoBruto.Text;
        }

        private void cboxListaConceptos_SelectedValueChanged(object sender, EventArgs e)
        {
        }

        private void cboxListaConceptos_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            int aux = 0;            

            if (this.txtSueldoBruto.Text == "") { return; }
            if (e.CurrentValue == CheckState.Unchecked)
            {
                if (conceptos[cboxListaConceptos.SelectedIndex].Signo == 1)
                {
                    descuentos += int.Parse(cboxListaConceptos.SelectedValue.ToString());
                    conceptosSeleccionados.Add(conceptos[cboxListaConceptos.SelectedIndex]);
                    aux = int.Parse(txtSueldoBruto.Text) * int.Parse(cboxListaConceptos.SelectedValue.ToString()) / 100;
                    txtSueldoNeto.Text = (int.Parse(txtSueldoNeto.Text) - aux).ToString();
                }
                else if (conceptos[cboxListaConceptos.SelectedIndex].Signo == 0)
                {
                    bonificacion += int.Parse(cboxListaConceptos.SelectedValue.ToString());
                    conceptosSeleccionados.Add(conceptos[cboxListaConceptos.SelectedIndex]);
                    aux = int.Parse(txtSueldoBruto.Text) * int.Parse(cboxListaConceptos.SelectedValue.ToString()) / 100;
                    txtSueldoNeto.Text = (int.Parse(txtSueldoNeto.Text) + aux).ToString();
                }
            }
            else if (e.CurrentValue == CheckState.Checked)
            {
                if (conceptos[cboxListaConceptos.SelectedIndex].Signo == 1)
                {
                    descuentos -= int.Parse(cboxListaConceptos.SelectedValue.ToString());
                    aux = int.Parse(txtSueldoBruto.Text) * int.Parse(cboxListaConceptos.SelectedValue.ToString()) / 100;
                    txtSueldoNeto.Text = (int.Parse(txtSueldoNeto.Text) + aux).ToString();
                    conceptosSeleccionados.Remove(conceptos[cboxListaConceptos.SelectedIndex]);
                }
                else if (conceptos[cboxListaConceptos.SelectedIndex].Signo == 0)
                {
                    bonificacion -= int.Parse(cboxListaConceptos.SelectedValue.ToString());
                    aux = int.Parse(txtSueldoBruto.Text) * int.Parse(cboxListaConceptos.SelectedValue.ToString()) / 100;
                    txtSueldoNeto.Text = (int.Parse(txtSueldoNeto.Text) - aux).ToString();
                    conceptosSeleccionados.Remove(conceptos[cboxListaConceptos.SelectedIndex]);
                }
            }
            txtDescuentos.Text = descuentos.ToString();
            txtBonificacion.Text = bonificacion.ToString();
            if(conceptosSeleccionados.Count == 0)
            {
                txtSueldoNeto.Text = txtSueldoBruto.Text;
            }
        }

        private void Enlazar()
        {
            List<Empleado> empleados = new List<Empleado>();
            empleados = Empleado.Listar();
            cboxEmpleados.DataSource = empleados;
            cboxEmpleados.DisplayMember = "NombreApellido";
            cboxEmpleados.ValueMember = "Id";
            cboxEmpleados.SelectedIndex = 0;

            conceptos = Concepto.Listar();
            ((ListBox)cboxListaConceptos).DataSource = conceptos;
            ((ListBox)cboxListaConceptos).DisplayMember = "Descripcion";
            ((ListBox)cboxListaConceptos).ValueMember = "Porcentaje";
            ((ListBox)lboxPorcentajes).DataSource = conceptos;
            ((ListBox)lboxPorcentajes).DisplayMember = "Porcentaje";
        }
    }
}
