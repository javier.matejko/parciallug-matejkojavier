﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Negocio;
using ParcialPractico.Recibos;

namespace ParcialPractico
{
    public partial class frmModificarRecibo : Form
    {
        private Recibo recibo;
        public frmModificarRecibo(Recibo r)
        {
            InitializeComponent();
            recibo = r;
        }
        private void frmModificarRecibo_Load(object sender, EventArgs e)
        {
            txtSueldoBruto.Text = recibo.SueldoBruto.ToString();
            txtSueldoNeto.Text = recibo.SueldoNeto.ToString();
            dtpFecha.Value = recibo.Fecha;
            cboxEmpleados.Items.Add(recibo.Id_Empleado);//modificar para que agarre el nombre del cliente
            
            Enlazar();
        }
        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            cboxEmpleados.SelectedIndex = 0;
            txtSueldoBruto.Text = txtSueldoNeto.Text = "";
            dtpFecha.Value = DateTime.Today;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

        }

        private void Enlazar()
        {
            List<Empleado> empleados = new List<Empleado>();
            empleados = Empleado.Listar();
            cboxEmpleados.DataSource = empleados;
            cboxEmpleados.DisplayMember = "NombreApellido";
            cboxEmpleados.ValueMember = "Id";
            cboxEmpleados.SelectedIndex = 0;

            List<Concepto> conceptos = new List<Concepto>();
            conceptos = Concepto.Listar();
            ((ListBox)cboxListaConceptos).DataSource = conceptos;
            ((ListBox)cboxListaConceptos).DisplayMember = "Descripcion";
            ((ListBox)cboxListaConceptos).ValueMember = "Porcentaje";
            ((ListBox)lboxPorcentajes).DataSource = conceptos;
            ((ListBox)lboxPorcentajes).DisplayMember = "Porcentaje";
            cboxListaConceptos.SelectedIndex = 0;
        }

        private void txtSueldoBruto_TextChanged(object sender, EventArgs e)
        {
            txtSueldoNeto.Text = txtSueldoBruto.Text;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            frmAgregarConcepto frmAgregarConcepto = new frmAgregarConcepto();
            frmAgregarConcepto.ShowDialog();
            lstConceptos.Items.Add(frmAgregarConcepto.concepto.Descripcion);
        }

        private void btnLimpiarItems_Click(object sender, EventArgs e)
        {

        }
    }
}
