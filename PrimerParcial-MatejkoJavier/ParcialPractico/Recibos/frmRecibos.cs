﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Negocio;

namespace ParcialPractico
{
    public partial class frmRecibos : Form
    {
        private int rowSeleccionado = 0;
        private int empleadoSeleccionado = 0;
        public frmRecibos()
        {
            InitializeComponent();
        }

        private void frmRecibos_Load(object sender, EventArgs e)
        {
            Enlazar();
            this.WindowState = FormWindowState.Maximized;
        }

        private void btNuevoRecibo_Click(object sender, EventArgs e)
        {
            frmNuevoRecibo ne = new frmNuevoRecibo();
            ne.ShowDialog();
            Enlazar();
        }
        private void Enlazar()
        {
            List<Empleado> empleados = new List<Empleado>();
            empleados = Empleado.Listar();
            cboxEmpleados.DataSource = empleados;
            cboxEmpleados.DisplayMember = "NombreApellido";
            cboxEmpleados.ValueMember = "Id";
            cboxEmpleados.SelectedIndex = 0;

            grdListaRecibos.DataSource = null;
            List<Recibo> recibos = new List<Recibo>();
            recibos = Recibo.Listar();
            grdListaRecibos.DataSource = recibos;

            grdConceptos.DataSource = null;
        }

        private void btnBuscarRecibos_Click(object sender, EventArgs e)
        {
            grdListaRecibos.DataSource = null;
            List<Recibo> recibos = new List<Recibo>();
            if (chkTodos.Checked)
            {
                recibos = Recibo.Listar();
            }
            else
            {
                recibos = Recibo.Listar(int.Parse(cboxEmpleados.SelectedValue.ToString()));
            }
            grdListaRecibos.DataSource = recibos;
            if(recibos.Count == 0)
            {
                grdConceptos.DataSource = null;
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            cboxEmpleados.SelectedIndex = 0;
            chkTodos.Checked = false;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(rowSeleccionado == 0 || empleadoSeleccionado == 0) { MessageBox.Show("Seleccione un recibo válido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            else
            {
                Recibo recibo = new Recibo();
                recibo.Id = rowSeleccionado;
                recibo.Id_Empleado = empleadoSeleccionado;
                if (recibo.Eliminar() >= 1)
                {
                    MessageBox.Show("Se ha eliminado el recibo con éxito!", "Empleado", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("Hubo un error eliminando el recibo", "Atención!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                Enlazar();
                
            }            
        }

        private void grdListaRecibos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            rowSeleccionado = int.Parse(grdListaRecibos.Rows[e.RowIndex].Cells[0].Value.ToString());
            empleadoSeleccionado = int.Parse(grdListaRecibos.Rows[e.RowIndex].Cells[7].Value.ToString());

            grdConceptos.DataSource = Concepto.Listar(empleadoSeleccionado, rowSeleccionado);
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            List<Recibo> recibos = new List<Recibo>();
            recibos = Recibo.ListarModificacion(rowSeleccionado);
            frmModificarRecibo frmModificarRecibo = new frmModificarRecibo(recibos[0]);
            frmModificarRecibo.ShowDialog();
            Enlazar();
        }
    }
}
