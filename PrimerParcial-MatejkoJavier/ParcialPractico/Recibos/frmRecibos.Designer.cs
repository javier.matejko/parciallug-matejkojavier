﻿namespace ParcialPractico
{
    partial class frmRecibos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btNuevoRecibo = new System.Windows.Forms.Button();
            this.grdListaRecibos = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.cboxEmpleados = new System.Windows.Forms.ComboBox();
            this.btnBuscarRecibos = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.chkTodos = new System.Windows.Forms.CheckBox();
            this.grdConceptos = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.grdListaRecibos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdConceptos)).BeginInit();
            this.SuspendLayout();
            // 
            // btNuevoRecibo
            // 
            this.btNuevoRecibo.Location = new System.Drawing.Point(573, 6);
            this.btNuevoRecibo.Name = "btNuevoRecibo";
            this.btNuevoRecibo.Size = new System.Drawing.Size(75, 23);
            this.btNuevoRecibo.TabIndex = 9;
            this.btNuevoRecibo.Text = "Nuevo";
            this.btNuevoRecibo.UseVisualStyleBackColor = true;
            this.btNuevoRecibo.Click += new System.EventHandler(this.btNuevoRecibo_Click);
            // 
            // grdListaRecibos
            // 
            this.grdListaRecibos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdListaRecibos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdListaRecibos.Location = new System.Drawing.Point(0, 35);
            this.grdListaRecibos.Name = "grdListaRecibos";
            this.grdListaRecibos.Size = new System.Drawing.Size(810, 415);
            this.grdListaRecibos.TabIndex = 8;
            this.grdListaRecibos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdListaRecibos_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Empleado:";
            // 
            // cboxEmpleados
            // 
            this.cboxEmpleados.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxEmpleados.FormattingEnabled = true;
            this.cboxEmpleados.Location = new System.Drawing.Point(93, 8);
            this.cboxEmpleados.Name = "cboxEmpleados";
            this.cboxEmpleados.Size = new System.Drawing.Size(239, 21);
            this.cboxEmpleados.TabIndex = 6;
            // 
            // btnBuscarRecibos
            // 
            this.btnBuscarRecibos.Location = new System.Drawing.Point(411, 6);
            this.btnBuscarRecibos.Name = "btnBuscarRecibos";
            this.btnBuscarRecibos.Size = new System.Drawing.Size(75, 23);
            this.btnBuscarRecibos.TabIndex = 5;
            this.btnBuscarRecibos.Text = "Buscar";
            this.btnBuscarRecibos.UseVisualStyleBackColor = true;
            this.btnBuscarRecibos.Click += new System.EventHandler(this.btnBuscarRecibos_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(492, 6);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 10;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(654, 6);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(75, 23);
            this.btnModificar.TabIndex = 11;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Visible = false;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(735, 6);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(75, 23);
            this.btnEliminar.TabIndex = 12;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // chkTodos
            // 
            this.chkTodos.AutoSize = true;
            this.chkTodos.Location = new System.Drawing.Point(349, 10);
            this.chkTodos.Name = "chkTodos";
            this.chkTodos.Size = new System.Drawing.Size(56, 17);
            this.chkTodos.TabIndex = 13;
            this.chkTodos.Text = "Todos";
            this.chkTodos.UseVisualStyleBackColor = true;
            // 
            // grdConceptos
            // 
            this.grdConceptos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdConceptos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdConceptos.Location = new System.Drawing.Point(809, 35);
            this.grdConceptos.Name = "grdConceptos";
            this.grdConceptos.Size = new System.Drawing.Size(301, 415);
            this.grdConceptos.TabIndex = 14;
            // 
            // frmRecibos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1110, 450);
            this.Controls.Add(this.grdConceptos);
            this.Controls.Add(this.chkTodos);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btNuevoRecibo);
            this.Controls.Add(this.grdListaRecibos);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboxEmpleados);
            this.Controls.Add(this.btnBuscarRecibos);
            this.Name = "frmRecibos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Recibos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmRecibos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdListaRecibos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdConceptos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btNuevoRecibo;
        private System.Windows.Forms.DataGridView grdListaRecibos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboxEmpleados;
        private System.Windows.Forms.Button btnBuscarRecibos;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.CheckBox chkTodos;
        private System.Windows.Forms.DataGridView grdConceptos;
    }
}