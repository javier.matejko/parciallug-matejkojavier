﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Negocio;

namespace ParcialPractico.Recibos
{
    public partial class frmAgregarConcepto : Form
    {
        List<Concepto> conceptos = new List<Concepto>();
        public Concepto concepto = new Concepto();
        public frmAgregarConcepto()
        {
            InitializeComponent();
        }

        private void frmAgregarConcepto_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void Enlazar()
        {            
            conceptos = Concepto.Listar();
            cboxConceptos.DataSource = conceptos;
            cboxConceptos.DisplayMember = "Descripcion";
            cboxConceptos.ValueMember = "Id";
            cboxConceptos.SelectedIndex = 0;            
        }

        private void cboxConceptos_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtPorcentaje.Text = conceptos[cboxConceptos.SelectedIndex].Porcentaje.ToString();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            concepto = conceptos[cboxConceptos.SelectedIndex];
            this.Close();
        }
    }
}
