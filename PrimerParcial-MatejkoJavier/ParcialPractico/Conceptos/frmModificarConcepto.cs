﻿using Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParcialPractico
{
    public partial class frmModificarConcepto : Form
    {
        private Concepto concepto;
        public frmModificarConcepto(Concepto c)
        {
            InitializeComponent();
            concepto = c;
        }
        private void frmModificarConcepto_Load(object sender, EventArgs e)
        {
            this.txtDescripcion.Text = concepto.Descripcion;
            this.txtPorcentaje.Text = concepto.Porcentaje.ToString();
            if(concepto.Signo == 0)
            {
                rbtnPositivo.Checked = true;
                rbtnNegativo.Checked = false;
            }
            else
            {
                rbtnPositivo.Checked = false;
                rbtnNegativo.Checked = true;
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtDescripcion.Text= txtPorcentaje.Text = "";
            rbtnPositivo.Checked = true;
            rbtnNegativo.Checked = false;

        }

        private void btnGuardarConcepto_Click(object sender, EventArgs e)
        {
            concepto.Descripcion = this.txtDescripcion.Text;
            concepto.Porcentaje = int.Parse(this.txtPorcentaje.Text.Replace("%", ""));
            if (concepto.Editar() == 1)
            {
                MessageBox.Show("Concepto modificado con éxito!", "Empleado", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("Hubo un error modificando el concepto", "Atención!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            this.Close();
        }
    }
}
