﻿namespace ParcialPractico
{
    partial class frmConceptos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btNuevoConcepto = new System.Windows.Forms.Button();
            this.grdListaConceptos = new System.Windows.Forms.DataGridView();
            this.btnBuscarConceptos = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdListaConceptos)).BeginInit();
            this.SuspendLayout();
            // 
            // btNuevoConcepto
            // 
            this.btNuevoConcepto.Location = new System.Drawing.Point(109, 6);
            this.btNuevoConcepto.Name = "btNuevoConcepto";
            this.btNuevoConcepto.Size = new System.Drawing.Size(75, 23);
            this.btNuevoConcepto.TabIndex = 14;
            this.btNuevoConcepto.Text = "Nuevo";
            this.btNuevoConcepto.UseVisualStyleBackColor = true;
            this.btNuevoConcepto.Click += new System.EventHandler(this.btNuevoConcepto_Click);
            // 
            // grdListaConceptos
            // 
            this.grdListaConceptos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdListaConceptos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdListaConceptos.Location = new System.Drawing.Point(0, 35);
            this.grdListaConceptos.Name = "grdListaConceptos";
            this.grdListaConceptos.Size = new System.Drawing.Size(833, 430);
            this.grdListaConceptos.TabIndex = 13;
            this.grdListaConceptos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdListaConceptos_CellClick);
            // 
            // btnBuscarConceptos
            // 
            this.btnBuscarConceptos.Location = new System.Drawing.Point(12, 6);
            this.btnBuscarConceptos.Name = "btnBuscarConceptos";
            this.btnBuscarConceptos.Size = new System.Drawing.Size(75, 23);
            this.btnBuscarConceptos.TabIndex = 10;
            this.btnBuscarConceptos.Text = "Buscar";
            this.btnBuscarConceptos.UseVisualStyleBackColor = true;
            this.btnBuscarConceptos.Click += new System.EventHandler(this.btnBuscarConceptos_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(207, 6);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(75, 23);
            this.btnModificar.TabIndex = 15;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(302, 6);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(75, 23);
            this.btnEliminar.TabIndex = 16;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // frmConceptos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(833, 465);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btNuevoConcepto);
            this.Controls.Add(this.grdListaConceptos);
            this.Controls.Add(this.btnBuscarConceptos);
            this.Name = "frmConceptos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Conceptos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmConceptos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdListaConceptos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btNuevoConcepto;
        private System.Windows.Forms.DataGridView grdListaConceptos;
        private System.Windows.Forms.Button btnBuscarConceptos;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnEliminar;
    }
}