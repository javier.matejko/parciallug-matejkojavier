﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Negocio;

namespace ParcialPractico
{
    public partial class frmConceptos : Form
    {
        private int rowSeleccionado = 0;
        public frmConceptos()
        {
            InitializeComponent();
        }
        private void frmConceptos_Load(object sender, EventArgs e)
        {
            Enlazar();
            this.WindowState = FormWindowState.Maximized;
        }
        private void btNuevoConcepto_Click(object sender, EventArgs e)
        {
            frmNuevoConcepto frmNuevoConcepto = new frmNuevoConcepto();
            frmNuevoConcepto.ShowDialog();
            Enlazar();
        }

        private void btnBuscarConceptos_Click(object sender, EventArgs e)
        {
            grdListaConceptos.DataSource = null;
            List<Concepto> conceptos = new List<Concepto>();
            conceptos = Concepto.Listar();
            grdListaConceptos.DataSource = conceptos;
        }

        private void Enlazar()
        {
            grdListaConceptos.DataSource = null;
            List<Concepto> conceptos = new List<Concepto>();
            conceptos = Concepto.Listar();
            grdListaConceptos.DataSource = conceptos;
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (rowSeleccionado == 0) { MessageBox.Show("Seleccione un concepto válido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            else
            {
                List<Concepto> conceptos = new List<Concepto>();
                conceptos = Concepto.Listar(rowSeleccionado);
                frmModificarConcepto frmModificarConcepto = new frmModificarConcepto(conceptos[0]);
                frmModificarConcepto.ShowDialog();
                Enlazar();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(rowSeleccionado == 0) { MessageBox.Show("Seleccione un concepto válido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            else
            {
                Concepto concepto = new Concepto();
                concepto.Id = rowSeleccionado;
                if (concepto.Eliminar() >= 1)
                {
                    MessageBox.Show("Se ha eliminado el concepto con éxito!", "Empleado", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("Hubo un error eliminando el concepto", "Atención!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                Enlazar();
            }            
        }

        private void grdListaConceptos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            rowSeleccionado = int.Parse(grdListaConceptos.Rows[e.RowIndex].Cells[0].Value.ToString());
        }
    }
}
