﻿using Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParcialPractico
{
    public partial class frmNuevoConcepto : Form
    {
        public frmNuevoConcepto()
        {
            InitializeComponent();            
        }
        private void frmNuevoConcepto_Load(object sender, EventArgs e)
        {
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtDescripcion.Text= txtPorcentaje.Text = "";
            rbtnPositivo.Checked = true;
            rbtnNegativo.Checked = false;

        }

        private void btnGuardarConcepto_Click(object sender, EventArgs e)
        {
            if(txtDescripcion.Text == "") { MessageBox.Show("Ingrese una descripción válida", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            else if (txtPorcentaje.Text.Length < 2) { MessageBox.Show("Ingrese un porcentaje válido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            else
            {
                Concepto concepto = new Concepto();
                concepto.Descripcion = this.txtDescripcion.Text;
                concepto.Porcentaje = int.Parse(this.txtPorcentaje.Text.Replace("%", ""));
                if(rbtnPositivo.Checked)
                {
                    concepto.Signo = 0;
                }
                else
                {
                    concepto.Signo = 1;
                }                

                if (concepto.Insertar() == 1)
                {
                    MessageBox.Show("Concepto cargado con éxito!", "Concepto", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("Hubo un error cargando el Concepto", "Atención!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                this.Close();
            }
            
        }
    }
}
