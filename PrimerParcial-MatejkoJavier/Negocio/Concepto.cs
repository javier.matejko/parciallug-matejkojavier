﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using AccesoDatos;

namespace Negocio
{
    public class Concepto
    {
        #region Variables
        private int id;
        private string descripcion;
        private int porcentaje;
        private int signo;
        #endregion
        #region Propiedades
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        public int Porcentaje
        {
            get { return porcentaje; }
            set { porcentaje = value; }
        }
        public int Signo
        {
            get { return signo; }
            set { signo = value; }
        }
        #endregion

        private Acceso acceso =  new Acceso();

        public int Insertar()
        {
            acceso.AbrirConexion();
            string sqlString = "Select ISNULL (MAX(id), 0) + 1 from Conceptos";

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id", acceso.LeerEscalar(sqlString)));
            parametros.Add(acceso.CrearParametro("@descripcion", this.descripcion));
            parametros.Add(acceso.CrearParametro("@porcentaje", this.porcentaje));
            parametros.Add(acceso.CrearParametro("@signo", this.signo));

            sqlString = "insert into Conceptos(Id, Descripcion, Porcentaje, Signo) values(@id, @descripcion, @porcentaje, @signo)";

            int resultado = acceso.Escribir(sqlString, parametros);

            acceso.CerrarConexion();

            return resultado;
        }

        public int Editar()
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@descripcion", this.descripcion));
            parametros.Add(acceso.CrearParametro("@porcentaje", this.porcentaje));
            parametros.Add(acceso.CrearParametro("@signo", this.signo));

            string sqlString = "Update Conceptos set Descripcion = @descripcion, " +
                                "Porcentaje = @porcentaje, " +
                                "Signo = @signo " +
                                "where Id = " + this.id;

            int resultado = acceso.Escribir(sqlString, parametros);

            acceso.CerrarConexion();

            return resultado;
        }

        public int Eliminar()
        {
            acceso.AbrirConexion();
            string sqlString = "Delete from Conceptos where id = @id";

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id", this.id));

            int resultado = acceso.Escribir(sqlString, parametros);

            acceso.CerrarConexion();

            return resultado;
        }
        public static List<Concepto> Listar()
        {
            List<Concepto> conceptos = new List<Concepto>();
            Acceso acceso = new Acceso();
            acceso.AbrirConexion();

            string sqlString = "select * from Conceptos";

            DataTable tabla = acceso.Leer(sqlString);
            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Concepto c = new Concepto();
                c.id = int.Parse(registro[0].ToString());
                c.descripcion = registro[1].ToString();
                c.porcentaje = int.Parse(registro[2].ToString());
                c.signo = int.Parse(registro[3].ToString());
                conceptos.Add(c);
            }

            return conceptos;
        }
        public static List<Concepto> Listar(int id)
        {
            List<Concepto> conceptos = new List<Concepto>();
            Acceso acceso = new Acceso();
            acceso.AbrirConexion();

            string sqlString = "select * from Conceptos where id = " + id.ToString();

            DataTable tabla = acceso.Leer(sqlString);
            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Concepto c = new Concepto();
                c.id = int.Parse(registro[0].ToString());
                c.descripcion = registro[1].ToString();
                c.porcentaje = int.Parse(registro[2].ToString());
                c.signo = int.Parse(registro[3].ToString());
                conceptos.Add(c);
            }

            return conceptos;
        }

        public static List<Concepto> Listar(int id_empleado, int id_recibo)
        {
            List<Concepto> conceptos = new List<Concepto>();
            Acceso acceso = new Acceso();
            acceso.AbrirConexion();
            
            string sqlString = "select * from Conceptos where id in (select Id_Concepto from Recibos_Detalle where Id_Empleado = "+ id_empleado.ToString() + " and Id_Recibo = "+ id_recibo.ToString() + ")";

            DataTable tabla = acceso.Leer(sqlString);
            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Concepto c = new Concepto();
                c.id = int.Parse(registro[0].ToString());
                c.descripcion = registro[1].ToString();
                c.porcentaje = int.Parse(registro[2].ToString());
                c.signo = int.Parse(registro[3].ToString());
                conceptos.Add(c);
            }

            return conceptos;
        }
    }
}
