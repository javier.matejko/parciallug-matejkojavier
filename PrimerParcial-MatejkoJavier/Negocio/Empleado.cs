﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AccesoDatos;

namespace Negocio
{
    public class Empleado
    {
        #region Constructores
        public Empleado() { }
        #endregion
        #region variables
        private int id;
        private string nombre;
        private string apellido;
        private string nombreApellido;
        private Int64 cuil;
        private Int64 legajo;
        private DateTime fechaAlta;
        #endregion
        #region Propiedades
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Nombre 
        { 
            get { return nombre; }
            set { nombre = value; }
        }
        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
        public string NombreApellido
        {
            get { return nombreApellido; }
            set { nombreApellido = value; }
        }
        public Int64 Cuil
        {
            get { return cuil; }
            set { cuil = value; }
        }
        public Int64 Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }
        public DateTime FechaAlta
        {
            get { return fechaAlta; }
            set { fechaAlta = value; }
        }
        #endregion

        private Acceso acceso = new Acceso();

        public int Insertar()
        {
            acceso.AbrirConexion();
            string sqlString = "Select ISNULL (MAX(id), 0) + 1 from Empleados";
            
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id", acceso.LeerEscalar(sqlString)));
            parametros.Add(acceso.CrearParametro("@nombre", this.nombre));
            parametros.Add(acceso.CrearParametro("@apellido", this.apellido));
            parametros.Add(acceso.CrearParametro("@cuil", this.cuil));
            parametros.Add(acceso.CrearParametro("@legajo", this.legajo));
            parametros.Add(acceso.CrearParametro("@fechaAlta", this.fechaAlta));
            parametros.Add(acceso.CrearParametro("@nombreApellido", this.nombreApellido));

            sqlString = "insert into Empleados(Id, Nombre, Apellido, CUIL, Legajo, FechaAlta, NombreApellido) values(@id, @nombre, @apellido, @cuil, @legajo, @fechaAlta, @nombreApellido)";

            int resultado = acceso.Escribir(sqlString, parametros);

            acceso.CerrarConexion();

            return resultado;
        }

        public int Editar()
        {
            acceso.AbrirConexion();            

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@nombre", this.nombre));
            parametros.Add(acceso.CrearParametro("@apellido", this.apellido));
            parametros.Add(acceso.CrearParametro("@cuil", this.cuil));
            parametros.Add(acceso.CrearParametro("@legajo", this.legajo));
            parametros.Add(acceso.CrearParametro("@fechaAlta", this.fechaAlta));
            parametros.Add(acceso.CrearParametro("@nombreApellido", this.nombreApellido));

            string sqlString = "Update Empleados set Nombre = @nombre, " +
                                "Apellido = @apellido, " +
                                "Cuil = @cuil, " +
                                "Legajo = @legajo, " +
                                "FechaAlta = @fechaAlta," +
                                "NombreApellido = @nombreApellido " +
                                "where Id = " + this.id;

            int resultado = acceso.Escribir(sqlString, parametros);

            acceso.CerrarConexion();

            return resultado;
        }

        public int Eliminar()
        {
            acceso.AbrirConexion();
            string sqlString = "Delete from Empleados where id = @id";

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id", this.id));

            int resultado = acceso.Escribir(sqlString, parametros);

            acceso.CerrarConexion();

            return resultado;
        }
        public static List<Empleado> Listar()
        {
            List<Empleado> empleados = new List<Empleado>();
            Acceso acceso = new Acceso();
            acceso.AbrirConexion();

            string sqlString = "select * from Empleados";

            DataTable tabla = acceso.Leer(sqlString);
            acceso.CerrarConexion();

            foreach(DataRow registro in tabla.Rows)
            {
                Empleado e = new Empleado();
                e.id = int.Parse(registro[0].ToString());
                e.nombre = registro[1].ToString();
                e.apellido = registro[2].ToString();
                e.cuil = Int64.Parse(registro[3].ToString());
                e.legajo = Int64.Parse(registro[4].ToString());
                e.fechaAlta = DateTime.Parse(registro[5].ToString());
                e.NombreApellido = registro[6].ToString();
                empleados.Add(e);
            }

            return empleados;
        }
        public static List<Empleado> Listar(int id)
        {
            List<Empleado> empleados = new List<Empleado>();
            Acceso acceso = new Acceso();
            acceso.AbrirConexion();

            string sqlString = "select * from Empleados where id = " + id.ToString();

            DataTable tabla = acceso.Leer(sqlString);
            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Empleado e = new Empleado();
                e.id = int.Parse(registro[0].ToString());
                e.nombre = registro[1].ToString();
                e.apellido = registro[2].ToString();
                e.cuil = Int64.Parse(registro[3].ToString());
                e.legajo = Int64.Parse(registro[4].ToString());
                e.fechaAlta = DateTime.Parse(registro[5].ToString());
                e.NombreApellido = registro[6].ToString();
                empleados.Add(e);
            }

            return empleados;
        }
        
    }
}
