﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using AccesoDatos;
using System.Runtime.CompilerServices;

namespace Negocio
{
    public class Recibo
    {
        #region Viariables
        private int id;
        private DateTime fecha;
        private int mes;
        private int año;
        private decimal sueldoBruto;
        private decimal sueldoNeto;
        private int descuentos;
        private int id_Empleado;
        private List<Concepto> conceptos;
        
        #endregion
        #region Propiedades
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public DateTime Fecha
        {
            get { return fecha; }
            set 
            { 
                fecha = value;
                mes = fecha.Month;
                año = fecha.Year;
            }
        }
        public int Mes
        {
            get { return mes; }
            set { mes = value; }
        }
        public int Año
        {
            get { return año; }
            set { año = value; }
        }
        public decimal SueldoBruto
        {
            get { return sueldoBruto; }
            set { sueldoBruto = value; }
        }
        public decimal SueldoNeto
        {
            get { return sueldoNeto; }
            set { sueldoNeto = value; }
        }
        public int Descuentos
        {
            get { return descuentos; }
            set { descuentos = value; }
        }
        public int Id_Empleado
        {
            get { return id_Empleado; }
            set { id_Empleado = value; }
        }
        public List<Concepto> Conceptos
        {
            get { return conceptos; }
            set { conceptos = value; }
        }
        #endregion

        private Acceso acceso =  new Acceso();
        private Recibo_Detalle recibo_Detalle = new Recibo_Detalle();

        public int Insertar()
        {
            acceso.AbrirConexion();
            string sqlString = "Select ISNULL (MAX(id), 0) + 1 from Recibos";

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            this.id = acceso.LeerEscalar(sqlString);
            parametros.Add(acceso.CrearParametro("@id", this.id));
            parametros.Add(acceso.CrearParametro("@id_Empleado", this.id_Empleado));
            parametros.Add(acceso.CrearParametro("@sueldoBruto", this.sueldoBruto));
            parametros.Add(acceso.CrearParametro("@sueldoNeto", this.sueldoNeto));
            parametros.Add(acceso.CrearParametro("@fecha", this.fecha));
            parametros.Add(acceso.CrearParametro("@mes", this.mes));
            parametros.Add(acceso.CrearParametro("@año", this.año));
            parametros.Add(acceso.CrearParametro("@descuentos", this.descuentos));

            sqlString = "insert into Recibos(Id, Id_Empleado, SueldoBruto, SueldoNeto, Fecha, Mes, Año, TotalDescuentos) " +
                                    "values(@id, @id_Empleado, @sueldoBruto, @sueldoNeto, @fecha, @mes, @año, @descuentos)";

            int resultado = acceso.Escribir(sqlString, parametros);

            resultado = recibo_Detalle.Insertar(conceptos, id_Empleado, id, fecha);

            acceso.CerrarConexion();

            return resultado;
        }

        public int Editar()
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@sueldoBruto", this.sueldoBruto));
            parametros.Add(acceso.CrearParametro("@sueldoNeto", this.sueldoNeto));
            parametros.Add(acceso.CrearParametro("@mes", this.mes));
            parametros.Add(acceso.CrearParametro("@año", this.año));
            parametros.Add(acceso.CrearParametro("@descuentos", this.descuentos));

            string sqlString = "Update Recibos set SueldoBruto = @sueldoBruto, " +
                                "SueldoNeto = @sueldoNeto, " +
                                "Mes = @mes, " +
                                "Año = @año, " +
                                "Descuentos = @descuentos " +
                                "where Id = " + this.id;

            int resultado = acceso.Escribir(sqlString, parametros);

            acceso.CerrarConexion();

            return resultado;
        }

        public int Eliminar()
        {
            acceso.AbrirConexion();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            string sqlString = "Delete from Recibos where Id = @id";

            parametros.Add(acceso.CrearParametro("@id", this.id));

            int resultado = recibo_Detalle.Eliminar(id_Empleado, id);

            resultado = acceso.Escribir(sqlString, parametros);

            acceso.CerrarConexion();

            return resultado;
        }
        public static List<Recibo> Listar()
        {
            List<Recibo> recibos = new List<Recibo>();
            Acceso acceso = new Acceso();
            acceso.AbrirConexion();

            string sqlString = "select * from Recibos";

            DataTable tabla = acceso.Leer(sqlString);
            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Recibo r = new Recibo();
                r.id = int.Parse(registro[0].ToString());
                r.Id_Empleado = int.Parse(registro[1].ToString());
                r.sueldoBruto = decimal.Parse(registro[2].ToString());
                r.sueldoNeto = decimal.Parse(registro[3].ToString());
                r.fecha = DateTime.Parse(registro[4].ToString());
                r.mes = int.Parse(registro[5].ToString());
                r.año = int.Parse(registro[6].ToString());
                r.descuentos = int.Parse(registro[7].ToString());
                recibos.Add(r);
            }

            return recibos;
        }
        public static List<Recibo> Listar(int id)
        {
            List<Recibo> recibos = new List<Recibo>();
            Acceso acceso = new Acceso();
            acceso.AbrirConexion();

            string sqlString = "select * from Recibos where Id_Empleado = " + id.ToString();

            DataTable tabla = acceso.Leer(sqlString);
            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Recibo r = new Recibo();
                r.id = int.Parse(registro[0].ToString());
                r.Id_Empleado = int.Parse(registro[1].ToString());
                r.sueldoBruto = decimal.Parse(registro[2].ToString());
                r.sueldoNeto = decimal.Parse(registro[3].ToString());
                r.fecha = DateTime.Parse(registro[4].ToString());
                r.mes = int.Parse(registro[5].ToString());
                r.año = int.Parse(registro[6].ToString());
                r.descuentos = int.Parse(registro[7].ToString());
                recibos.Add(r);
            }

            return recibos;
        }
        public static List<Recibo> ListarModificacion(int id)
        {
            List<Recibo> recibos = new List<Recibo>();
            Acceso acceso = new Acceso();
            acceso.AbrirConexion();

            string sqlString = "select * from Recibos where Id = " + id.ToString();

            DataTable tabla = acceso.Leer(sqlString);
            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Recibo r = new Recibo();
                r.id = int.Parse(registro[0].ToString());
                r.Id_Empleado = int.Parse(registro[1].ToString());
                r.sueldoBruto = decimal.Parse(registro[2].ToString());
                r.sueldoNeto = decimal.Parse(registro[3].ToString());
                r.fecha = DateTime.Parse(registro[4].ToString());
                r.mes = int.Parse(registro[5].ToString());
                r.año = int.Parse(registro[6].ToString());
                r.descuentos = int.Parse(registro[7].ToString());
                recibos.Add(r);
            }

            return recibos;
        }
    }
}
