﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;

using AccesoDatos;

namespace Negocio
{
    class Recibo_Detalle
    {
        #region Variables
        private int id;
        private int id_Empleado;
        private int id_Recibo;
        private int id_Concepto;
        private DateTime fecha;
        #endregion
        #region Propiedades
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public int Id_Empleado
        {
            get { return id_Empleado; }
            set { id_Empleado = value; }
        }
        public int Id_Recibo
        {
            get { return id_Recibo; }
            set { id_Recibo = value; }
        }
        public int Id_Concepto
        {
            get { return id_Concepto; }
            set { id_Concepto = value; }
        }
        public DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }
        #endregion

        private Acceso acceso = new Acceso();

        public int Insertar(List<Concepto> conceptos, int id_empleado, int id_recibo, DateTime _fecha)
        {
            acceso.AbrirConexion();
            int resultado = 0;
            string sqlString = "";
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            this.id_Empleado = id_empleado;
            this.id_Recibo = id_recibo;
            this.fecha = _fecha;

            foreach (Concepto c in conceptos)
            {
                this.id_Concepto = c.Id;
                sqlString = "Select ISNULL (MAX(id), 0) + 1 from Recibos_Detalle";
                parametros.Clear();
                parametros.Add(acceso.CrearParametro("@id", acceso.LeerEscalar(sqlString)));                
                parametros.Add(acceso.CrearParametro("@id_Empleado", this.id_Empleado));
                parametros.Add(acceso.CrearParametro("@id_Recibo", this.id_Recibo));
                parametros.Add(acceso.CrearParametro("@fecha", this.fecha));
                parametros.Add(acceso.CrearParametro("@id_Concepto", this.id_Concepto));                

                sqlString = "insert into Recibos_Detalle(Id, " +
                            "Id_Empleado, " +
                            "Id_Recibo, " +
                            "Id_Concepto, " +
                            "Fecha) " +
                            "values(@id, @id_Empleado, @id_Recibo, @id_Concepto, @fecha)";

                resultado = acceso.Escribir(sqlString, parametros);
            }
            acceso.CerrarConexion();

            return resultado;
        }

        //public int Editar()
        //{
        //    acceso.AbrirConexion();

        //    List<IDbDataParameter> parametros = new List<IDbDataParameter>();

        //    parametros.Add(acceso.CrearParametro("@sueldoBruto", this.sueldoBruto));
        //    parametros.Add(acceso.CrearParametro("@sueldoNeto", this.sueldoNeto));
        //    parametros.Add(acceso.CrearParametro("@mes", this.mes));
        //    parametros.Add(acceso.CrearParametro("@año", this.año));
        //    parametros.Add(acceso.CrearParametro("@descuentos", this.descuentos));

        //    string sqlString = "Update Recibos set SueldoBruto = @sueldoBruto, " +
        //                        "SueldoNeto = @sueldoNeto, " +
        //                        "Mes = @mes, " +
        //                        "Año = @año, " +
        //                        "Descuentos = @descuentos " +
        //                        "where Id = " + this.id;

        //    int resultado = acceso.Escribir(sqlString, parametros);

        //    acceso.CerrarConexion();

        //    return resultado;
        //}

        public int Eliminar(int id_empleado, int id_recibo)
        {
            acceso.AbrirConexion();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            this.id_Empleado = id_empleado;
            this.id_Recibo = id_recibo;
            string sqlString = "Delete from Recibos_Detalle where Id_Empleado = @id_Empleado and Id_Recibo =@id_Recibo";

            //parametros.Add(acceso.CrearParametro("@id_Empleado", this.id_Empleado));
            //parametros.Add(acceso.CrearParametro("@id_Recibo", this.id));

            //int resultado = acceso.Escribir(sqlString, parametros);

            parametros.Add(acceso.CrearParametro("@id_Empleado", this.id_Empleado));
            parametros.Add(acceso.CrearParametro("@id_Recibo", this.id_Recibo));

            int resultado = acceso.Escribir(sqlString, parametros);

            acceso.CerrarConexion();

            return resultado;
        }
        //public static List<Recibo> Listar()
        //{
        //    List<Recibo> recibos = new List<Recibo>();
        //    Acceso acceso = new Acceso();
        //    acceso.AbrirConexion();

        //    string sqlString = "select * from Recibos";

        //    DataTable tabla = acceso.Leer(sqlString);
        //    acceso.CerrarConexion();

        //    foreach (DataRow registro in tabla.Rows)
        //    {
        //        Recibo r = new Recibo();
        //        r.id = int.Parse(registro[0].ToString());
        //        r.Id_Empleado = int.Parse(registro[1].ToString());
        //        r.sueldoBruto = decimal.Parse(registro[2].ToString());
        //        r.sueldoNeto = decimal.Parse(registro[3].ToString());
        //        r.fecha = DateTime.Parse(registro[4].ToString());
        //        r.mes = int.Parse(registro[5].ToString());
        //        r.año = int.Parse(registro[6].ToString());
        //        r.descuentos = int.Parse(registro[7].ToString());
        //        recibos.Add(r);
        //    }

        //    return recibos;
        //}
        //public static List<Recibo> Listar(int id)
        //{
        //    List<Recibo> recibos = new List<Recibo>();
        //    Acceso acceso = new Acceso();
        //    acceso.AbrirConexion();

        //    string sqlString = "select * from Recibos where Id_Empleado = " + id.ToString();

        //    DataTable tabla = acceso.Leer(sqlString);
        //    acceso.CerrarConexion();

        //    foreach (DataRow registro in tabla.Rows)
        //    {
        //        Recibo r = new Recibo();
        //        r.id = int.Parse(registro[0].ToString());
        //        r.Id_Empleado = int.Parse(registro[1].ToString());
        //        r.sueldoBruto = decimal.Parse(registro[2].ToString());
        //        r.sueldoNeto = decimal.Parse(registro[3].ToString());
        //        r.fecha = DateTime.Parse(registro[4].ToString());
        //        r.mes = int.Parse(registro[5].ToString());
        //        r.año = int.Parse(registro[6].ToString());
        //        r.descuentos = int.Parse(registro[7].ToString());
        //        recibos.Add(r);
        //    }

        //    return recibos;
        //}
    }
}
